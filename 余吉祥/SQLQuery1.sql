create database Invoicing

use Invoicing

create table Stock --进货表
(
	StockID int primary key identity(1,1),
	ProductID varchar(20) not null,-- 商品编号
	ProductSort varchar(10) not null,--商品类别
	ProductName varchar(10) not null,--商品名称
	ProductCount int not null,--进货数量
	StockCost money  not null,--进货价格
	StockTime smalldatetime not null default(getdate())-- 进货时间
)



create table Sell --销售表
(
	SellID int primary key identity(1,1),
	ProductID varchar(20) not null,				--商品编号
	ProductSort varchar(10) not null,			--商品类别
	ProductName varchar(10) not null,			--商品名称
	SellCount int not null,						--销售数量
	SellCost money  not null,					--销售价格
	SellTime money not null default(getdate())	--销售时间

)

create table Inventory -- 库存表
(
InventoryId int identity(1,1) primary key,				
ProductSort varchar(10) not null,					--商品类别
ProductID varchar(20) not null,						--商品编号
InventoryQuantity int not null,						--库存数量
WarehouseEntryTime smalldatetime not null,          --入库时间
)

create table CommodityInformation --商品信息表
(
CommodityInformationId int primary key identity(1,1),
ProductID varchar(20) not null,						--商品编号
SupplierNumber nvarchar(30) not null,				--供应商编号
ProductIntroduction nvarchar(200),					--商品简介
)

create table StaffInformation --员工信息表
(
StaffInformationId int identity(1,1) primary key,
EmployeeId nvarchar(20) not null unique,			--员工编号
HomeAddress nvarchar(80) not null,					--家庭住址
ContactNumber varchar(11) not null,					--联系电话
													--员工姓名

)
